/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* global getAssetRegistry getFactory emit */

/**
 * Sample transaction processor function.
 * @param {org.example.basic.SampleTransaction} tx The sample transaction instance.
 * @transaction
 */
async function sampleTransaction(tx) {  // eslint-disable-line no-unused-vars

    if(Number(tx.asset.value) > Number(tx.newValue)){
        throw new Error("new value should be greater than old value");
    }

    // Save the old value of the asset.
    const oldValue = tx.asset.value;

    // Update the asset with the new value.
    tx.asset.value = tx.newValue;

    // Get the asset registry for the asset.
    const assetRegistry = await getAssetRegistry('org.example.basic.SampleAsset');
    // Update the asset in the asset registry.
    await assetRegistry.update(tx.asset);

    // Emit an event for the modified asset.
    let event = getFactory().newEvent('org.example.basic', 'SampleEvent');
    event.asset = tx.asset;
    event.oldValue = oldValue;
    event.newValue = tx.newValue;
    emit(event);
}

/**
 * Sample transaction processor function.
 * @param {org.example.basic.ChangeOwnership} tx The sample transaction instance.
 * @transaction
 */
async function changeOwnership(tx) {  // eslint-disable-line no-unused-vars
    const participantRegistry = await getParticipantRegistry('org.example.basic.SampleParticipant');
    const isNewOwnerRegistered = await participantRegistry.exists(tx.newOwner.participantId);
    
    // Check if new owner is present or not?
    if(!isNewOwnerRegistered){
        throw new Error ("Can't find new owner registered. Please register the owner first.");
    }

    // Check if transfer is between same owner.
    if (tx.asset.owner.participantId === tx.newOwner.participantId){
        throw new Error("Can't transfer to the same owner.")
    }

    // Save the old owner of the asset.
    const oldOwner = tx.asset.owner;

    // Update the asset with the new owner.
    tx.asset.owner = tx.newOwner;
    
    // Get the asset registry for the asset.
    const assetRegistry = await getAssetRegistry('org.example.basic.SampleAsset');
    // Update the asset in the asset registry.
    await assetRegistry.update(tx.asset);
}

